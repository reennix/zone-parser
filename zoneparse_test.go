package zoneparser

import (
	"fmt"
	"io/ioutil"
	"testing"
)

func GetRRFile(i int) string {
	zf, _ := ioutil.ReadFile(fmt.Sprintf("./TestData/RR%d", i))
	return string(zf)
}

func TestParseConsts(t *testing.T) {
	zs := newZoneScanner("$ORIGIN example.com.\n")
	z := Zone{}

	t.Log("Testing parseConsts ORIGIN")
	err := parseConsts(zs, &z)
	if err != nil {
		t.Fatalf("parseConts returned an error %s", err.Error())
	}
	if z.Origin != "example.com." {
		t.Fatalf("Zone const not parsed correctly. Got '%s', expected 'example.com.'", z.Origin)
	}
}

func TestParseConsts2(t *testing.T) {
	zs := newZoneScanner("$TTL 300\n")
	z := Zone{}

	t.Log("Testing parseConsts TTL")
	err := parseConsts(zs, &z)
	if err != nil {
		t.Fatalf("parseConsts returned an error %s", err.Error())
	}
	if z.Ttl != 300 {
		t.Fatalf("Zone const not parsed correctly. Get '%d', expected '300'", z.Ttl)
	}
}

func TestParseRR(t *testing.T) {
	zs := newZoneScanner(GetRRFile(1))
	z := Zone{
		Origin: "example.com.",
		SOA: &ZoneSOA{
			Email:      "amin.example.com.",
			Nameserver: "ns1.example.com.",
			Serial:     "1",
			Refresh:    "2",
			Retry:      "3",
			Expire:     "4",
			Minimum:    "5",
		},
		Ttl: 300,
	}
	ps := ParseState{}
	record, err := parseRR(zs, &z, &ps)
	if err != nil {
		t.Fatalf("parseRR returned an error %s", err.Error())
	}
	if record != nil {
		t.Fatal("parseRR returned record that is not SOA")
	}

	record = &z.Records[0]
	if record.Host != "@" {
		t.Fatalf("Hostname is '%s', but '%s' was expected", record.Host, z.Origin)
	}
	if record.Ttl != 86400 {
		t.Fatalf("Record TTL is '%d', but '86400' was expected", record.Ttl)
	}
	if record.Type != "AAAA" {
		t.Fatalf("Record type is '%s', but 'AAAA' was expected", record.Type)
	}
	if record.Class != "IN" {
		t.Fatalf("Record class is '%s', but 'IN' was expected", record.Type)
	}
	if record.Content != "2001:db8::1" {
		t.Fatalf("Record content is '%s', but '2001:db8::1' was expected", record.Content)
	}
}

func TestParseRR2(t *testing.T) {
	zs := newZoneScanner(GetRRFile(2))
	z := Zone{
		Origin: "example.com.",
		SOA: &ZoneSOA{
			Email:      "amdin.example.com.",
			Nameserver: "ns1.example.com.",
			Serial:     "1",
			Refresh:    "2",
			Retry:      "3",
			Expire:     "4",
			Minimum:    "5",
		},
		Ttl: 300,
	}
	ps := ParseState{}
	record, err := parseRR(zs, &z, &ps)
	if err != nil {
		t.Fatalf("parseRR returned an error %s", err.Error())
	}
	if record == nil {
		t.Fatal("parseRR didn't return an SOA record")
	}

	if record.Host != "@" {
		t.Fatalf("Hostname is '%s', but '%s' was expected", record.Host, z.Origin)
	}
	if record.Ttl != 86400 {
		t.Fatalf("Record TTL is '%d', but '86400' was expected", record.Ttl)
	}
	if record.Type != "SOA" {
		t.Fatalf("Record type is '%s', but 'SOA' was expected", record.Type)
	}
	if record.Class != "IN" {
		t.Fatalf("Record class is '%s', but 'IN' was expected", record.Type)
	}
	if record.Content != "admin.test. test. 1 2 3 4 5" {
		t.Fatalf("Record content is '%s', but 'admin.test. test. 1 2 3 4 5' was expected", record.Content)
	}
}

func TestParseSOAContent(t *testing.T) {
	r := &Record{
		Host:    "@",
		Content: "nameserver email 1 2 3 4 5",
	}
	soa := parseSOAContent(r)
	if soa.Nameserver != "nameserver" {
		t.Fatalf("Expected nameserver to be 'nameserver', but got '%s'", soa.Nameserver)
	}
	if soa.Email != "email" {
		t.Fatalf("Expected nameserver to be 'email', but got '%s'", soa.Email)
	}
	if soa.Serial != "1" {
		t.Fatalf("Expected serial to be '1', but got '%s'", soa.Serial)
	}
	if soa.Refresh != "2" {
		t.Fatalf("Expected refresh to be '2', but got '%s'", soa.Refresh)
	}
	if soa.Retry != "3" {
		t.Fatalf("Expected retry to be '3', but got '%s'", soa.Retry)
	}
	if soa.Expire != "4" {
		t.Fatalf("Expected expire to be '4', but got '%s'", soa.Expire)
	}
	if soa.Minimum != "5" {
		t.Fatalf("Expected minimum to be '5', but got '%s'", soa.Minimum)
	}
}

func TestParseZone(t *testing.T) {
	zone, err := ParseZone(GetZoneFile(1))
	if err != nil {
		t.Fatalf("ParseZone returned an error %s", err.Error())
	}
	if zone.SOA.Nameserver != "ns1.example.com." {
		t.Fatalf("Expected SOA Nameserver to be 'ns1.example.com.', but got '%s'", zone.SOA.Nameserver)
	}
	if zone.SOA.Email != "admin.example.com." {
		t.Fatalf("Expected SOA Email to be 'admin.example.com.', but got '%s'", zone.SOA.Email)
	}
}

func TestParseZoneFile2(t *testing.T) {
	file := GetZoneFile(2)
	zone, err := ParseZone(string(file))
	if err != nil {
		t.Fatalf("ParseZone returned an error %s", err.Error())
	}
	controlFile, _ := ioutil.ReadFile("TestData/ZoneFile2_Out")
	generatedZone, err := zone.ToZoneFile()
	if err != nil {
		t.Fatalf("ToZoneFile returned an error %s", err.Error())
	}
	if string(controlFile) != generatedZone {
		t.Fatal("Written zone does not match expected output")
	}
}

func TestParseZoneFile3(t *testing.T) {
	file := GetZoneFile(3)
	zone, err := ParseZone(string(file))
	if err != nil {
		t.Fatalf("ParseZone returned an error %s", err.Error())
	}
	controlFile, _ := ioutil.ReadFile("TestData/ZoneFile3_Out")
	generatedZone, err := zone.ToZoneFile()
	if err != nil {
		t.Fatalf("ToZoneFile returned an error %s", err.Error())
	}
	if string(controlFile) != generatedZone {
		t.Fatal("Written zone does not match expected output")
	}
}

func TestTtlToUint(t *testing.T) {
	ttl, err := ttlToUint("10m")
	if err != nil {
		t.Fatalf("Error: %s", err.Error())
	}
	if ttl != 600 {
		t.Fatalf("Got invalid TTL")
	}

	ttl, err = ttlToUint("10h")
	if err != nil {
		t.Fatalf("Error: %s", err.Error())
	}
	if ttl != 36000 {
		t.Fatalf("Got invalid TTL")
	}

	ttl, err = ttlToUint("10d")
	if err != nil {
		t.Fatalf("Error: %s", err.Error())
	}
	if ttl != 864000 {
		t.Fatalf("Got invalid TTL")
	}

	ttl, err = ttlToUint("300")
	if err != nil {
		t.Fatalf("Error: %s", err.Error())
	}
	if ttl != 300 {
		t.Fatalf("Got invalid TTL")
	}
}
