# Zone Parser

Zone Parser is a simple to use Go library that can parse bind like
DNS-Zones.


## Reading existing zone files

```Go
func main() {
	zoneFile, _ := ioutil.ReadFile("myzonefile")
	zone := ParseZone(string(zoneFile))
}
```

## Writing a Zone to a file

Note that `ToZoneFile` will not preserve the format of zones that where
parsed by `ParseZone`.

```Go
func main() {
	zoneFile, _ := ioutil.ReadFile("myzonefile")
	zone := ParseZone(string(zoneFile))
	zoneFile = zone.ToZoneFile() // Returns zone as string
}
```

## Updating the serial

Zone Parser can update the serial of your zone. There are two differend
methods of serial updating available. You can use `Zone.UpdateSerial`
for updating the serial with the current UTC UNIX timestamp or
`Zone.UpdateSerialRFC` to update your zone with the RFC compliant serial
representation.

``` Go
func updateSerial(z *Zone) {
		z.UpdateSerial() // z.SOA.Serial -> UNIX timestamp
		z.UpdateSerialRFC() // z.SOA.Serial -> RFC compliant representation (yyyymmdd00)
}
```
