package zoneparser

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// ParseState represents the current state of the zone parser. ParseState is used by parseZone
// and does not need to be used manually.
type ParseState struct {
	Parenthesis  bool
	Quote        bool
	PreviousName string
}

// ParseZone parses a bind-like zone file and returns
// a Zone struct.
func ParseZone(bindZone string) (*Zone, error) {
	s := newZoneScanner(bindZone)
	zone := Zone{}
	ps := ParseState{
		Parenthesis: false,
	}
	for s.HasNext() {
		nextChar := s.Peek(1)

		switch {
		case nextChar == ";":
			s.SkipToEoL()
			s.Next(1)
		case nextChar == "$":
			err := parseConsts(s, &zone)
			if err != nil {
				return nil, err
			}
		case nextChar == "\n":
			s.Next(1)
		default:
			record, err := parseRR(s, &zone, &ps)
			if err != nil {
				return nil, err
			}
			if record != nil {
				zone.SOA = parseSOAContent(record)
			}
		}
	}
	return &zone, nil
}

// parseRR parses the next single RR and appends the record
// to the zone. If the record is an SOA it will be returned instead.
// This is normally invoked by parseZone  and does not need to be
// invoked manually.
func parseRR(s *ZoneScanner, z *Zone, p *ParseState) (*Record, error) {
	record := Record{}

	parseHostname(&record, s, p)
	p.PreviousName = record.Host
	err := parseTTL(z, &record, s)
	if err != nil {
		return nil, err
	}
	parseClass(&record, s)
	parseRecordType(&record, s)
	parseRecordContent(&record, s, p)

	if record.Type != "SOA" {
		z.Records = append(z.Records, record)
		return nil, nil
	}
	return &record, nil
}

// parseRecordContent parses the content of the current record
func parseRecordContent(r *Record, s *ZoneScanner, p *ParseState) {
	for s.HasNext() {
		nextChar := s.Next(1)
		switch {
		case nextChar == "(":
			p.Parenthesis = true
		case nextChar == ")":
			p.Parenthesis = false
		case nextChar == "\"":
			p.Quote = !p.Quote
			r.Content += nextChar
		case p.Parenthesis && !p.Quote && nextChar == ";":
			s.SkipToEoL()
			s.Next(1)
		case !p.Parenthesis && !p.Quote && nextChar == ";":
			s.SkipToEoL()
			s.Next(1)
			s.SkipWhitespace()
			goto endLoop
		case p.Parenthesis && nextChar == "\n":
			s.Next(1)
			s.SkipWhitespace()
		case !p.Parenthesis && nextChar == "\n":
			goto endLoop
		case nextChar == " " || nextChar == "\t":
			s.SkipWhitespace()
			if !(s.Peek(1) == ";") {
				r.Content += " "
			}
		default:
			r.Content += nextChar
		}
	}
endLoop:
	r.Content = strings.TrimSpace(r.Content)
}

// parseRecordType parses the type of the current record
func parseRecordType(r *Record, s *ZoneScanner) {
	// Parse record type
	for s.HasNext() {
		nextChar := s.Next(1)
		if nextChar == " " || nextChar == "\t" {
			break
		}
		r.Type += nextChar
	}
	s.SkipWhitespace()
}

// parseClass parses the class type of the current record
func parseClass(r *Record, s *ZoneScanner) {
	for s.HasNext() {
		nextChar := s.Next(1)
		if nextChar == " " || nextChar == "\t" {
			break
		}
		r.Class += nextChar
	}
	s.SkipWhitespace()
}

// parseTTL parses the time to live of the current RR
func parseTTL(z *Zone, r *Record, s *ZoneScanner) error {
	ttl := ""
	for s.HasNext() {
		re := regexp.MustCompile("[0-9mhd]")
		if !re.MatchString(s.Peek(1)) {
			r.Ttl = z.Ttl
			break
		}
		nextChar := s.Next(1)
		if nextChar == " " || nextChar == "\t" {
			break
		}
		ttl += nextChar
	}
	if ttl != "" {
		ttlUint, err := ttlToUint(ttl)
		if err != nil {
			return fmt.Errorf("invalid ttl: %s", ttl)
		}
		r.Ttl = ttlUint
	}
	s.SkipWhitespace()
	return nil
}

// ttlToUint converts a TTL to an uint.
// TTLs can have the following synatx:
// 000[<m|h|d>]
func ttlToUint(ttl string) (uint, error) {
	re := regexp.MustCompile("^\\d+[mhd]?$")
	if !re.MatchString(ttl) {
		return 0, fmt.Errorf("expected ttl, but got '%s'", ttl)
	}

	re = regexp.MustCompile("^\\d+")
	f := re.FindString(ttl)
	t, err := strconv.ParseUint(f, 10, 32)
	if err != nil {
		return 0, err
	}

	re = regexp.MustCompile("[mhd]$")
	f = re.FindString(ttl)

	switch f {
	case "m":
		return uint(t * 60), nil
	case "h":
		return uint(t * 60 * 60), nil
	case "d":
		return uint(t * 60 * 60 * 24), nil
	}

	return uint(t), nil
}

//  parseHostname parses the hostname of the current RR
func parseHostname(r *Record, s *ZoneScanner, p *ParseState) {
	if s.Peek(1) == " " || s.Peek(1) == "\t" {
		r.Host = p.PreviousName
		s.SkipWhitespace()
		return
	}

	for s.HasNext() {
		nextChar := s.Next(1)
		if nextChar == " " || nextChar == "\t" {
			break
		}
		r.Host += nextChar
	}
	s.SkipWhitespace()
}

// parseGeneratorExpression parses a generator expression from the
// provided ZoneScanner. Range limitations are currently not enforced.
// The following expressions are supported:
// $GENERATE <min>-<max> <record>
// $GENERATE <min>-<max>/<step> <record>
func parseGeneratorExpressions(s *ZoneScanner, z *Zone) error {
	if s.Peek(10) != "$GENERATE " {
		return errors.New("not a generator expression")
	}
	s.Next(10)

	// Get expression
	line := ""
	for s.HasNext() {
		nextChar := s.Next(1)
		if nextChar == "\n" {
			break
		}
		line += nextChar
	}

	stepRe := regexp.MustCompile("^\\d+-\\d+/\\d+")
	hasStep := stepRe.MatchString(line)
	step := 1
	re := regexp.MustCompile("\\d+")
	fg := re.FindAllString(line, 3)
	min, err := strconv.Atoi(fg[0])
	if err != nil {
		return fmt.Errorf("minimum is not an integer value: %s", fg[0])
	}
	max, err := strconv.Atoi(fg[1])
	if err != nil {
		return fmt.Errorf("maximum is not an integer value: %s", fg[1])
	}

	if hasStep {
		step, err = strconv.Atoi(fg[2])
		if err != nil {
			return fmt.Errorf("step is not an integer value: %s", fg[2])
		}
	}

	wsRegex := regexp.MustCompile("[ |\t]")
	line = line[wsRegex.FindStringIndex(line)[0]+1:]

	for i := min; i <= max; i += step {
		ps := ParseState{}
		genLine := strings.ReplaceAll(line, "$", fmt.Sprint(i))
		ls := newZoneScanner(genLine)
		_, err := parseRR(ls, z, &ps)
		if err != nil {
			return err
		}
	}

	return nil
}

// parseConsts parses the constants in a zone file. There is normally no
// need to call this manually. Use parseZone instead.
func parseConsts(s *ZoneScanner, z *Zone) error {
	if s.Peek(1) != "$" {
		return nil
	}

	if s.Peek(10) == "$GENERATE " {
		err := parseGeneratorExpressions(s, z)
		if err != nil {
			return err
		}
		return nil
	}

	s.Next(1)
	// Parse const name
	name := ""
	for s.HasNext() {
		nextChar := s.Next(1)

		if nextChar == " " || nextChar == "\t" {
			break
		}
		name += nextChar
	}
	s.SkipWhitespace()

	// Parse content of const
	content := ""
	for s.HasNext() {
		nextChar := s.Next(1)
		if nextChar == ";" || nextChar == " " {
			s.SkipToEoL()
			continue
		}
		if nextChar == " " || nextChar == "\t" {
			s.SkipToEoL()
			continue
		}
		if nextChar == "\n" {
			break
		}
		content += nextChar
	}

	if name == "ORIGIN" {
		z.Origin = content
	} else if name == "TTL" {
		ttl, err := ttlToUint(content)
		if err != nil {
			return fmt.Errorf("ttl is not an integer value: %s", content)
		}
		z.Ttl = uint(ttl)
	} else {
		panic(fmt.Sprintf("Fatal - Unknown constant in zone file: %s", name))
	}
	return nil
}

// parseSOAContent parses the content of an already formatted SOA record.
// If you use this manually make sure it has the following format:
// <nameserver> <email> <serial> <refresh> <retry> <expire> <minimum>
func parseSOAContent(soaRecord *Record) *ZoneSOA {
	c := soaRecord.Content
	s := newZoneScanner(c)
	r := ZoneSOA{}
	r.Host = soaRecord.Host

	// Parse nameserver
	buf := ""
	for s.HasNext() {
		nextChar := s.Next(1)
		if nextChar == " " || !s.HasNext() {
			switch {
			case r.Nameserver == "":
				r.Nameserver = buf
			case r.Email == "":
				r.Email = buf
			case r.Serial == "":
				r.Serial = buf
			case r.Refresh == "":
				r.Refresh = buf
			case r.Retry == "":
				r.Retry = buf
			case r.Expire == "":
				r.Expire = buf
			case r.Minimum == "":
				r.Minimum = buf + nextChar
			}
			buf = ""
			continue
		}
		buf += nextChar
	}

	return &r
}
