package zoneparser

import "testing"

func TestUtil_reverseString(t *testing.T) {
	str := "abcde"
	str = reverseString(str)
	if str != "edcba" {
		t.Fatal("String not reversed")
	}
}
