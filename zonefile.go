package zoneparser

import (
	"bytes"
	_ "embed"
	"errors"
	"fmt"
	"net/netip"
	"strconv"
	"strings"
	"text/tabwriter"
	template2 "text/template"
	"time"
)

//go:embed zone.template
var zoneTemplate string

// Record represents a single DNS resource record
type Record struct {
	Class   string
	Content string
	Host    string
	Ttl     uint
	Type    string
}

// ZoneSOA represents an SOA record
type ZoneSOA struct {
	Host       string
	Email      string
	Nameserver string
	Serial     string
	Refresh    string
	Retry      string
	Expire     string
	Minimum    string
}

// Zone represents a DNS zone. It can either be instantiated manually
// or by using ToZoneFile
type Zone struct {
	SOA     *ZoneSOA
	Origin  string
	Records []Record
	Ttl     uint
}

// ToZoneFile returns a string containing a bind-like zone file from the
// provided zone
func (z *Zone) ToZoneFile() (string, error) {
	template, err := template2.New("zonefile").Parse(zoneTemplate)
	if err != nil {
		panic(err)
	}

	var buff bytes.Buffer
	err = template.Execute(&buff, z)
	if err != nil {
		return "", err
	}

	var outBuf bytes.Buffer
	tabWriter := tabwriter.NewWriter(&outBuf, 0, 1, 4, ' ', 0)
	_, err = tabWriter.Write(buff.Bytes())
	if err != nil {
		return "", err
	}
	return outBuf.String(), nil
}

// UpdateSerial updates the serial number of the record with the current
// Unix time stamp.
// Note: This does not follow the official recommendation for zone
// serial numbers. Use UpdateSerialRFC instead, if you want RFC
// compliant serial numbers.
func (z *Zone) UpdateSerial() {
	z.SOA.Serial = fmt.Sprint(time.Now().Unix())
}

// UpdateSerialRFC updates the serial number of the record with the
// current date and a two digit serial.
func (z *Zone) UpdateSerialRFC() error {
	oldSerial := z.SOA.Serial
	t := time.Now()
	newSerial := t.Format("20060102")

	if newSerial == oldSerial[:8] {
		serial, err := strconv.Atoi(oldSerial[8:10])
		if err != nil {
			return err
		}
		if serial == 99 {
			return errors.New("already maximum serial for the day")
		}
		serial += 1
		newSerial = newSerial + fmt.Sprintf("%02d", serial)
		z.SOA.Serial = newSerial
		return nil
	}

	z.SOA.Serial = newSerial + "01"
	return nil
}

// AddPTR adds a creates a PTR from an IP address and adds it to the
// zone. If Ttl is 0 the default TTl from the zone is being used.
// Note: Legacy IP is currently not supported.
func (z *Zone) AddPTR(IP string, Host string, Ttl uint) error {
	ipaddr, err := netip.ParseAddr(IP)
	if err != nil {
		return err
	}

	if !ipaddr.Is6() || strings.Contains(IP, "%") {
		return fmt.Errorf("not an ipv6 address or link specific: '%s'", IP)
	}

	expIP := ipaddr.StringExpanded()
	expIP = strings.ReplaceAll(expIP, ":", "")
	ptrStr := ""

	for _, s := range expIP {
		ptrStr = string(s) + "." + ptrStr
	}

	ptrStr += "ip6.arpa."
	ptr := Record{
		Class:   "IN",
		Content: ptrStr,
		Host:    Host,
		Ttl:     Ttl,
		Type:    "PTR",
	}

	if Ttl == 0 {
		ptr.Ttl = z.Ttl
	}

	z.Records = append(z.Records, ptr)
	return nil
}
