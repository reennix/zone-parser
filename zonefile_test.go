package zoneparser

import (
	"testing"
	"time"
)

func TestZone_AddPTR(t *testing.T) {
	zone := Zone{
		SOA:     nil,
		Origin:  "",
		Records: nil,
		Ttl:     600,
	}

	err := zone.AddPTR("2001:db8::1", "test.example.com.", 300)
	if err != nil {
		t.Fatalf("AddPTR returned an error: %s", err.Error())
	}
	record := zone.Records[0]
	if record.Content != "1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.8.b.d.0.1.0.0.2.ip6.arpa." {
		t.Fatalf("Record content does not match expected content: '%s'", record.Content)
	}
	if record.Ttl != 300 {
		t.Fatalf("Record TTL does not match expected TTL: %d", record.Ttl)
	}

	err = zone.AddPTR("2001:db8::109.0.2.1", "test2.example.com.", 0)
	if err != nil {
		t.Fatalf("AddPTR returned an error: %s", err.Error())
	}

	record = zone.Records[1]
	if record.Content != "1.0.2.0.0.0.d.6.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.8.b.d.0.1.0.0.2.ip6.arpa." {
		t.Fatalf("Record content does not match expected record: %s'", record.Content)
	}
	if record.Host != "test2.example.com." {
		t.Fatalf("Record host does not match expected record host: '%s'", record.Host)
	}
	if record.Ttl != zone.Ttl {
		t.Fatalf("Record TTL does not match expected TTL: %d", record.Ttl)
	}
}

func TestUpdateSerialRFC(t *testing.T) {
	z := Zone{
		SOA: &ZoneSOA{
			Serial: "20220411802",
		},
	}

	err := z.UpdateSerialRFC()
	if err != nil {
		t.Fatalf("UpdateSErialRFC returned an error: %s", err.Error())
	}

	if z.SOA.Serial != time.Now().Format("20060102")+"01" {
		t.Fatalf("Serial does not match expeced serial: '%s'", z.SOA.Serial)
	}
}
