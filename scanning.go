package zoneparser

// ZoneScanner is a simple scanner that provides functionality
// for zone files. It should not be instantiated manually, instead
// use newZoneScanner
type ZoneScanner struct {
	content string
	pos     int
}

// newZoneScanner returnes a new ZoneScanner with the zones
// content.
func newZoneScanner(zone string) *ZoneScanner {
	return &ZoneScanner{
		content: zone,
		pos:     0,
	}
}

// HasNext returns whether the scanner has a next char.
func (z *ZoneScanner) HasNext() bool {
	return len(z.content) > z.pos
}

// Peek Returns the next n chars of the scanners content
// without changing the position. If n is greater than the remaining
// content, only the remaining content will be returned.
func (z *ZoneScanner) Peek(n int) string {
	if z.pos+n > len(z.content) {
		n = len(z.content) - z.pos
	}

	return z.content[z.pos : z.pos+n]
}

// Next Returns the next n chars of the scanners content
// and sets the new position. If n is greater than the remaining
// content, only the remaining content will be returned.
func (z *ZoneScanner) Next(n int) string {
	if z.pos+n > len(z.content) {
		n = len(z.content) - z.pos
	}
	result := z.content[z.pos : z.pos+n]
	z.pos += n
	return result
}

// SkipWhitespace goes to the next char that is not whitespace
func (z *ZoneScanner) SkipWhitespace() {
	for z.Peek(1) == " " || z.Peek(1) == "\t" {
		z.Next(1)
	}
}

// SkipToEoL goes to the end of the line
func (z *ZoneScanner) SkipToEoL() {
	for z.Peek(1) != "\n" {
		z.Next(1)
	}
}
