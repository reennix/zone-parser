package zoneparser

// reverseString reverses the given string
func reverseString(s string) string {
	var buf string
	for _, i := range s {
		buf = string(i) + buf
	}
	return buf
}
