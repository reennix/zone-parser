package zoneparser

import (
	"fmt"
	"io/ioutil"
	"testing"
)

func GetZoneFile(i int) string {
	zf, _ := ioutil.ReadFile(fmt.Sprintf("./TestData/ZoneFile%d", i))
	return string(zf)
}

func TestZoneScanner_HasNext(t *testing.T) {
	t.Log("Testing ZoneScanner.HasNext")
	zf := GetZoneFile(1)
	zs := newZoneScanner(string(zf))
	if !zs.HasNext() {
		t.Fatal("HasNext returned false")
	}

	zs.Next(1000)
	if zs.HasNext() {
		t.Fatal("HasNext returned true")
	}
}

func TestZoneScanner_Peak(t *testing.T) {
	t.Log("Testing ZoneScanner.Peek")
	zf := GetZoneFile(1)
	zs := newZoneScanner(zf)
	zp := zs.Peek(2)
	if zp != "$O" {
		t.Fatalf("Peek returned '%s' instead of '$O'", zp)
	}
	zs.Next(452)
	zp = zs.Peek(10)
	if zp != "8::2" {
		t.Fatalf("Peek returned '%s' instead of '8::2'", zp)
	}
}

func TestZoneScanner_Next(t *testing.T) {
	t.Log("Testing ZoneScanner.Next")
	zf := GetZoneFile(1)
	zs := newZoneScanner(zf)
	zn := zs.Next(2)
	if zn != "$O" {
		t.Fatalf("Next returned '%s' instead of '$O'", zn)
	}
	zs.Next(450)
	zn = zs.Next(10)
	if zn != "8::2" {
		t.Fatalf("Next returned '%s' instead of '8::2'", zn)
	}
}
